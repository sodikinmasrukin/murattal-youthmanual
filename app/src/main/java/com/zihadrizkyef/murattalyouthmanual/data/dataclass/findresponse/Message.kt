package com.zihadrizkyef.murattalyouthmanual.data.dataclass.findresponse

data class Message(
    val body: Body,
    val header: Header
)