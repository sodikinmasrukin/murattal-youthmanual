package com.zihadrizkyef.murattalyouthmanual.data.dataclass.findresponse

data class MusicGenre(
    val music_genre: MusicGenreX
)