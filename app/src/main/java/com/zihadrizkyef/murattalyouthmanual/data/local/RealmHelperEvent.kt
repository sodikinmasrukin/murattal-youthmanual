package com.zihadrizkyef.murattalyouthmanual.data.local

import android.content.Context
import com.zihadrizkyef.murattalyouthmanual.data.dataclass.CalendarEvent
import io.realm.Realm

class RealmHelperEvent(private val context: Context) {
    private val realm: Realm by lazy {
        Realm.init(context)
        Realm.getDefaultInstance()
    }

    fun getAllEventList(): ArrayList<CalendarEvent> {
        val result = realm.where(CalendarEvent::class.java).findAll()
        return if (result.isEmpty()) {
            arrayListOf()
        } else {
            val list = arrayListOf<CalendarEvent>()
            list.addAll(result)
            list
        }
    }

    fun getEventListOn(day: Int, month: Int, year: Int): ArrayList<CalendarEvent> {
        val result = realm.where(CalendarEvent::class.java)
            .equalTo("year", year)
            .equalTo("month", month)
            .equalTo("day", day)
            .findAll()
        return if (result.isEmpty()) {
            arrayListOf()
        } else {
            val list = arrayListOf<CalendarEvent>()
            list.addAll(result)
            list
        }
    }

    fun addEvent(event: CalendarEvent) {
        realm.beginTransaction()
        realm.copyToRealm(event)
        realm.commitTransaction()
    }
}