package com.zihadrizkyef.murattalyouthmanual.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.zihadrizkyef.murattalyouthmanual.R
import com.zihadrizkyef.murattalyouthmanual.data.local.RealmHelperEvent
import com.zihadrizkyef.murattalyouthmanual.ui.addeventcustomview.AddEventCustomView
import com.zihadrizkyef.murattalyouthmanual.ui.addeventcustomview.AddEventListener
import com.zihadrizkyef.murattalyouthmanual.ui.calendarcustomview.CalendarCustomView
import com.zihadrizkyef.murattalyouthmanual.ui.findcustomview.FetchingDataListener
import com.zihadrizkyef.murattalyouthmanual.ui.findcustomview.FindCustomView
import com.zihadrizkyef.murattalyouthmanual.ui.wishlistcustomview.WishlistCustomView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {
    val fetchingDataListener = FetchingDataListener()
    private lateinit var eventRealm : RealmHelperEvent
    private lateinit var googleSignInClient: GoogleSignInClient

    private lateinit var findCustomView: FindCustomView
    private lateinit var wishlistCustomView: WishlistCustomView
    private lateinit var addEventCustomView: AddEventCustomView
    private lateinit var calendarCustomView: CalendarCustomView
    private lateinit var activeView: View

    private val onCreateEventListener = object: AddEventListener {
        override fun onCreateSuccess() {
            val calendarItem = navBottom.menu.getItem(2)
            if (!calendarItem.isChecked) {
                supportActionBar?.title = "Calendar"
                activeView.visibility = View.GONE
                calendarCustomView.visibility = View.VISIBLE
                activeView = calendarCustomView
                calendarItem.isChecked = true
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        eventRealm = RealmHelperEvent(this)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(this, gso)

        navBottom.setOnNavigationItemSelectedListener(this)
        navBottom.inflateMenu(R.menu.bottom_nav_menu)

        findCustomView = FindCustomView(context = this, idlingResource = fetchingDataListener)
        wishlistCustomView = WishlistCustomView(this)
        addEventCustomView = AddEventCustomView(this).apply { onCreateEventListener = this@MainActivity.onCreateEventListener }
        calendarCustomView = CalendarCustomView(this)
        customViewContainer.addView(findCustomView)
        customViewContainer.addView(wishlistCustomView)
        customViewContainer.addView(calendarCustomView)
        customViewContainer.addView(addEventCustomView)
        wishlistCustomView.visibility = View.GONE
        calendarCustomView.visibility = View.GONE
        addEventCustomView.visibility = View.GONE
        activeView = findCustomView

        etSearch.setText("Mishary")
        findCustomView.searchTrack("Mishary")

        supportActionBar?.title = "Find"

        if (intent.hasExtra(AddEventCustomView.KEY_EVENT_INDEX)) {
            val eventIndex = intent.getIntExtra(AddEventCustomView.KEY_EVENT_INDEX, 1)
            val event = eventRealm.getAllEventList()[eventIndex]

            activeView.visibility = View.GONE
            calendarCustomView.visibility = View.VISIBLE
            activeView = calendarCustomView

            calendarCustomView.setSelectedDate(event.day, event.month, event.year)
            intent.removeExtra(AddEventCustomView.KEY_EVENT_INDEX)
            val calendarItem = navBottom.menu.getItem(2)
            calendarItem.isChecked = true
        }

        btClear.setOnClickListener {
            if (etSearch.text.isNotBlank()) {
                etSearch.setText("")
                showKeyboardAt(etSearch)
            }
        }

        etSearch.setOnEditorActionListener { _, _, _ ->
            fetchingDataListener.isFetchingData = true
            if (etSearch.text.isNotBlank()) {
                hideKeyboard(this)
                findCustomView.searchTrack(etSearch.text.toString())
            } else {
                fetchingDataListener.isFetchingData = false
            }
            true
        }

        etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                if (s!!.toString().isNotBlank()) {
                    btClear.setImageResource(R.drawable.ic_clear)
                } else {
                    btClear.setImageResource(R.drawable.ic_search)
                }
            }
        })
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.navFind) {
            searchBox.visibility = View.VISIBLE
            toolbar.visibility = View.GONE
        } else {
            searchBox.visibility = View.GONE
            toolbar.visibility = View.VISIBLE

            hideKeyboard(this)
        }

        when (item.itemId) {
            R.id.navLogout -> {
                showDialogLogout()
            }

            R.id.navFind -> {
                if (!item.isChecked) {
                    findCustomView.visibility = View.VISIBLE
                    activeView.visibility = View.GONE
                    activeView = findCustomView
                    supportActionBar?.title = "Find"
                    showKeyboardAt(etSearch)
                    return true
                }
            }

            R.id.navWishlist -> {
                if (!item.isChecked) {
                    activeView.visibility = View.GONE
                    wishlistCustomView.visibility = View.VISIBLE
                    activeView = wishlistCustomView
                    supportActionBar?.title = "Wishlist"
                    return true
                }
            }

            R.id.navCalendar -> {
                if (!item.isChecked) {
                    activeView.visibility = View.GONE
                    calendarCustomView.visibility = View.VISIBLE
                    activeView = calendarCustomView
                    supportActionBar?.title = "Calendar"
                    return true
                }
            }

            R.id.navCreate -> {
                if (!item.isChecked) {
                    activeView.visibility = View.GONE
                    addEventCustomView.visibility = View.VISIBLE
                    activeView = addEventCustomView
                    supportActionBar?.title = "Create"
                    return true
                }
            }
        }

        return false
    }

    private fun showDialogLogout() {
        AlertDialog.Builder(this)
            .setTitle("Peringatan")
            .setMessage("Apakah anda yakin ingin keluar?")
            .setNegativeButton("Batal") {_, _ -> }
            .setPositiveButton("Yakin") {_, _ -> logoutAccount()}
            .show()
    }

    private fun logoutAccount() {
        googleSignInClient.signOut()
        LoginManager.getInstance().logOut()

        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    private fun hideKeyboard(context: Context) {
        val view = (context as AppCompatActivity).currentFocus
        if (view != null) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm!!.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun showKeyboardAt(editText: EditText) {
        editText.requestFocus()
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm!!.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
    }
}
