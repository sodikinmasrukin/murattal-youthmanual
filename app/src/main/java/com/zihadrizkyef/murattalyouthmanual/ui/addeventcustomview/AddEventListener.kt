package com.zihadrizkyef.murattalyouthmanual.ui.addeventcustomview

interface AddEventListener {
    fun onCreateSuccess()
}