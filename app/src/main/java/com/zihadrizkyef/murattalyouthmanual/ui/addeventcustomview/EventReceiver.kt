package com.zihadrizkyef.murattalyouthmanual.ui.addeventcustomview

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import com.zihadrizkyef.murattalyouthmanual.R
import com.zihadrizkyef.murattalyouthmanual.data.local.RealmHelperEvent
import com.zihadrizkyef.murattalyouthmanual.ui.MainActivity

class EventReceiver : BroadcastReceiver() {
    companion object {
        private const val NOTIFICATION_ID = 64752
        private const val NOTIFICATION_CHANNEL_ID = "667647"
    }

    private var eventIndex = 0
    private var eventName = ""
    private lateinit var eventRealm: RealmHelperEvent

    override fun onReceive(context: Context, intent: Intent) {
        eventRealm = RealmHelperEvent(context)
        eventIndex = intent.getIntExtra(AddEventCustomView.KEY_EVENT_INDEX, 0)
        eventName = eventRealm.getAllEventList()[eventIndex].name

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            createNotificationBelowOreo(context)
        } else {
            createNotificationAboveOreo(context)
        }
    }

    private fun createNotificationBelowOreo(context: Context) {
        val intent = Intent(context, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(AddEventCustomView.KEY_EVENT_INDEX, eventIndex)
        val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val notification = NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Event Reminder")
                .setContentText(eventName)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)

        val notificationManager = NotificationManagerCompat.from(context)
        notificationManager.notify(NOTIFICATION_ID, notification.build())
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun createNotificationAboveOreo(context: Context) {
        val name = context.getString(R.string.app_name)
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance)
        val notificationManager = context.getSystemService(NotificationManager::class.java)
        notificationManager!!.createNotificationChannel(channel)

        val intent = Intent(context, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(AddEventCustomView.KEY_EVENT_INDEX, eventIndex)
        val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val notification = NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Event Reminder")
                .setContentText(eventName)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)

        notificationManager.notify(NOTIFICATION_ID, notification.build())
    }
}
