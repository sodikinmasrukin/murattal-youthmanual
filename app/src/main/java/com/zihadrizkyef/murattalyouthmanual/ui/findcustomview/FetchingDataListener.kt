package com.zihadrizkyef.murattalyouthmanual.ui.findcustomview

import androidx.test.espresso.IdlingResource

class FetchingDataListener : IdlingResource {
    var isFetchingData = false
        set(value) {
            field = value
            if (!value) {
                resourceCallback?.onTransitionToIdle()
            }
        }
    var resourceCallback: IdlingResource.ResourceCallback? = null

    override fun getName(): String {
        return this.javaClass.name
    }

    override fun isIdleNow(): Boolean {
        return !isFetchingData
    }

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback?) {
        resourceCallback = callback
    }
}