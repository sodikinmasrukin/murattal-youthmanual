package com.zihadrizkyef.murattalyouthmanual.ui.calendarcustomview

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.LinearLayoutCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.AttributeSet
import android.view.View
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.zihadrizkyef.murattalyouthmanual.R
import com.zihadrizkyef.murattalyouthmanual.data.dataclass.CalendarEvent
import com.zihadrizkyef.murattalyouthmanual.data.local.RealmHelperEvent
import kotlinx.android.synthetic.main.customview_calendar.view.*

class CalendarCustomView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : LinearLayoutCompat(context, attrs, defStyleAttr) {

    private var eventAdapter: EventAdapter
    private val eventList = arrayListOf<CalendarEvent>()
    private val eventRealm = RealmHelperEvent(context)

    init {
        View.inflate(context, R.layout.customview_calendar, this)

        eventAdapter = EventAdapter(context, eventList)

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = eventAdapter
        calendarView.selectedDate = CalendarDay.today()
        calendarView.setOnDateChangedListener { _, date, _ ->
            eventList.clear()
            eventList.addAll(eventRealm.getEventListOn(date.day, date.month, date.year))
            eventAdapter.notifyDataSetChanged()
        }
    }

    override fun setVisibility(visibility: Int) {
        super.setVisibility(visibility)

        highlightEvents()
        updateRecyclerView()
    }

    private fun updateRecyclerView() {
        val date = calendarView.selectedDate!!
        eventList.clear()
        eventList.addAll(eventRealm.getEventListOn(date.day, date.month, date.year))
        eventAdapter.notifyDataSetChanged()
    }

    fun highlightEvents() {
        val days = arrayListOf<CalendarDay>()
        eventRealm.getAllEventList().forEach { days.add(CalendarDay.from(it.year, it.month, it.day)) }

        val todayHighlighter = OneDayDecorator()
        val eventHighlighter = EventDecorator(Color.parseColor("#70FF0000"), days)
        calendarView.removeDecorators()
        calendarView.addDecorators(todayHighlighter, eventHighlighter)
    }

    fun setSelectedDate(day: Int, month: Int, year: Int) {
        calendarView.selectedDate = CalendarDay.from(year, month, day)
    }
}
