package com.zihadrizkyef.murattalyouthmanual.ui

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.facebook.*
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.zihadrizkyef.murattalyouthmanual.R
import com.zihadrizkyef.murattalyouthmanual.data.local.SharedPref
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    companion object {
        private const val RC_GOOGLE_LOGIN = 4446
    }

    private lateinit var googleSignInClient: GoogleSignInClient
    private val fbCallbackManager = CallbackManager.Factory.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        googleSignInClient= GoogleSignIn.getClient(this, gso)
        btGoogleLogin.setOnClickListener {
            performGoogleLogin()
        }

        btFbLogin.setPermissions("email")
        btFbLogin.registerCallback(fbCallbackManager, object: FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                handleFBSignResult(result)
            }
            override fun onCancel() {}
            override fun onError(error: FacebookException?) {}
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        fbCallbackManager.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_GOOGLE_LOGIN) {
            val signInData = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleGoogleSignResult(signInData)
        }
    }

    private fun performGoogleLogin() {
        val intent = googleSignInClient.signInIntent
        startActivityForResult(intent, RC_GOOGLE_LOGIN)
    }

    private fun handleGoogleSignResult(signInData: Task<GoogleSignInAccount>?) {
        try {
            val account = signInData!!.getResult(ApiException::class.java)
            SharedPref(this).email = account!!.email!!

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        } catch (e: ApiException) {
            e.printStackTrace()
        }

    }

    private fun handleFBSignResult(result: LoginResult?) {
        GraphRequest.newMeRequest(result!!.accessToken) { json, _ ->
            val email = json.optString("email")
            SharedPref(this).email = email

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }.executeAsync()
    }
}
