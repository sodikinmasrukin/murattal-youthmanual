package com.zihadrizkyef.murattalyouthmanual

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.zihadrizkyef.murattalyouthmanual.RecyclerViewMatcher.Companion.withItemCount
import com.zihadrizkyef.murattalyouthmanual.ui.MainActivity
import com.zihadrizkyef.murattalyouthmanual.ui.addeventcustomview.AddEventCustomView
import com.zihadrizkyef.murattalyouthmanual.ui.calendarcustomview.CalendarCustomView
import com.zihadrizkyef.murattalyouthmanual.ui.findcustomview.FindCustomView
import com.zihadrizkyef.murattalyouthmanual.ui.wishlistcustomview.WishlistCustomView
import org.hamcrest.Matchers.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityEspressoTest {
    companion object {
        private const val LOGOUT_DIALOG_TITLE = "Peringatan"
        private const val TRACK_FOUND_ARTIST = "Bandar Baleela"
        private const val TRACK_NOT_FOUND_ARTIST = "haousaeoustaoeuhtsshtaueothnsaeou"
    }

    private lateinit var idlingResource: IdlingResource

    @Before
    fun init() {
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
        activityScenario.onActivity {
            idlingResource = it.fetchingDataListener
            IdlingRegistry.getInstance().register(idlingResource)
        }
    }

    @Test
    fun navigationFindClick_ChangeContentView() {
        onView(withId(R.id.navFind)).perform(click())
        onView(instanceOf(FindCustomView::class.java)).check(matches(isDisplayed()))
    }

    @Test
    fun navigationWishlistClick_ChangeContentView() {
        onView(withId(R.id.navWishlist)).perform(click())
        onView(instanceOf(WishlistCustomView::class.java)).check(matches(isDisplayed()))
    }

    @Test
    fun navigationCalendarClick_ChangeContentView() {
        onView(withId(R.id.navCalendar)).perform(click())
        onView(instanceOf(CalendarCustomView::class.java)).check(matches(isDisplayed()))
    }

    @Test
    fun navigationCreateClick_ChangeContentView() {
        onView(withId(R.id.navCreate)).perform(click())
        onView(instanceOf(AddEventCustomView::class.java)).check(matches(isDisplayed()))
    }

    @Test
    fun navigationLogoutClick_ChangeContentView() {
        onView(withId(R.id.navLogout)).perform(click())
        onView(withText(LOGOUT_DIALOG_TITLE)).check(matches(isDisplayed()))
    }

    @Test
    fun findTrackFound() {
        onView(withId(R.id.navFind))
            .perform(click())
        onView(withId(R.id.etSearch))
            .perform(clearText(), typeText(TRACK_FOUND_ARTIST), pressImeActionButton())

        onView(allOf(withId(R.id.recyclerView), isDescendantOfA(instanceOf(FindCustomView::class.java))))
            .check(matches(hasDescendant(withText(containsString(TRACK_FOUND_ARTIST)))))
        onView(allOf(withId(R.id.tvEmptyRv), isDescendantOfA(instanceOf(FindCustomView::class.java))))
            .check(matches(not(isDisplayed())))
    }

    @Test
    fun findTrackNotFound() {
        onView(withId(R.id.navFind))
            .perform(click())
        onView(withId(R.id.etSearch))
            .perform(clearText(), typeText(TRACK_NOT_FOUND_ARTIST), pressImeActionButton())

        onView(allOf(withId(R.id.recyclerView), isDescendantOfA(instanceOf(FindCustomView::class.java))))
            .check(withItemCount(0))
        onView(allOf(withId(R.id.tvEmptyRv), isDescendantOfA(instanceOf(FindCustomView::class.java))))
            .check(matches(isDisplayed()))
    }

    @After
    fun onFinish() {
        IdlingRegistry.getInstance().unregister(idlingResource)
    }
}